import tkinter as tk
from tkinter import ttk
import tkFileDialog
from tkinter.scrolledtext import ScrolledText


class App(tk.Frame):

    def __init__(self, *args, **kwargs):

        # Currently:
        #   - Creates a window (tk.Frame)
        #   - Creates the rest of the gui implemented in __createWidgets()
        tk.Frame.__init__(self, *args, **kwargs)
        self.__createWidgets()

    # -- Create rest of gui below here -- #
    def __createWidgets(self):

        # variable to store filename
        self.filename = ""

        # variable to set content intro text if desired
        self.contenttext = ""

        # Currently: Creates label, entry, button, and scrolled-text widgets
        #            for user to insert content
        self.TitleLabel = ttk.Label(text="Blog Entry Title: ")
        self.Title = tk.StringVar()
        self.TitleEntry = ttk.Entry(textvariable=self.Title)
        self.TitleButton = tk.Button(self, text='Set Title', command=self.getTitleEntry)
        self.TitleLabel.pack(anchor=tk.W)
        self.TitleEntry.pack(fill=tk.X, anchor=tk.W)

        self.DateLabel = ttk.Label(text="Blog Entry Date: ")
        self.Date = tk.StringVar()
        self.DateEntry = ttk.Entry(textvariable=self.Date)
        self.DateButton = tk.Button(self, text='Set Date', command=self.getDateEntry)
        self.DateLabel.pack(anchor=tk.W)
        self.DateEntry.pack(fill=tk.X, anchor=tk.W)

        self.TopicLabel = ttk.Label(text="Blog Entry Topic: ")
        self.Topic = tk.StringVar()
        self.TopicEntry = ttk.Entry(textvariable=self.Topic)
        self.TopicButton = tk.Button(self, text='Set Topic', command=self.getTopicEntry)
        self.TopicLabel.pack(anchor=tk.W)
        self.TopicEntry.pack(fill=tk.X, anchor=tk.W)

        self.ContentLabel = ttk.Label(text="Blog Entry Content: ")
        self.Content = ScrolledText(self, width=75, height=15)
        self.Content.insert(tk.INSERT, self.contenttext)
        self.ContentLabel.pack(anchor=tk.W)
        self.Content.pack(fill=tk.X, anchor=tk.S)

        tk.Button(self, text='Choose an Image: ',
                  command=self.getImageFilePath).pack(fill=tk.X, anchor=tk.W)

        self.TitleButton.pack(fill=tk.X, anchor=tk.W)
        self.DateButton.pack(fill=tk.X, anchor=tk.W)
        self.TopicButton.pack(fill=tk.X, anchor=tk.W)

        tk.Button(self, text='Set Content',
                  command=self.getContentScrolledText).pack(fill=tk.X, anchor=tk.W)

    # Currently: Creates a function that stores the content from the various
    #            Entry Fields (ex: Title, Date, Topic, and Content) and inserts
    #            into templated test file.

    def getTitleEntry(self):
        title = self.TitleEntry.get()
        titleFormatted = '<h2 class="title is-2">{}</h2>'.format(title)
        with open("testfile.html", "r+") as f:
            a = [x.rstrip() for x in f]
            index = 0
            for item in a:
                if item.startswith("<!-- T"):
                    a.insert(index, titleFormatted)
                    break
                index += 1
            f.seek(0)
            f.truncate()
            for line in a:
                f.write(line + "\n")

    def getDateEntry(self):
        date =  self.DateEntry.get()
        with open("testfile.html", "r+") as f:
            a = [x.rstrip() for x in f]
            index = 0
            for item in a:
                if item.startswith("<!-- D"):
                    a.insert(index, date)
                    break
                index += 1
            f.seek(0)
            f.truncate()
            for line in a:
                f.write(line + "\n")

    def getTopicEntry(self):
        topic = self.TopicEntry.get()
        with open("testfile.html", "r+") as f:
            a = [x.rstrip() for x in f]
            index = 0
            for item in a:
                if item.startswith("<!-- TO"):
                    a.insert(index, topic)
                    break
                index += 1
            f.seek(0)
            f.truncate()
            for line in a:
                f.write(line + "\n")

    def getContentScrolledText(self):
        fetched_content = self.Content.get('1.0', 'end-1c')
        fetched_content_formatted = '<p>{}</p>'.format(fetched_content)
        with open("testfile.html", "r+") as f:
            a = [x.rstrip() for x in f]
            index = 0
            for item in a:
                if item.startswith("<!-- C"):
                    a.insert(index, fetched_content_formatted)
                    break
                index += 1
            f.seek(0)
            f.truncate()
            for line in a:
                f.write(line + "\n")

    # Currently: Creates a function that opens a file browser window and allows
    #            the user to choose an image to add to the blog post
    # TODO: Allow the user to attach otherthings than just images.
    #      - Files, videos, and sounds as hopeful future options
    def getImageFilePath(self):
        fetched_filename = self.filename = tkFileDialog.askopenfilename(title="Open file")
        fetched_filename_formatted = '<img style="padding: 10px 2px" src="{}" alt="">'.format(fetched_filename)
        with open("testfile.html", "r+") as f:
            a = [x.rstrip() for x in f]
            index = 0
            for item in a:
                if item.startswith("<!-- I"):
                    a.insert(index, fetched_filename_formatted)
                    break
                index += 1
            f.seek(0)
            f.truncate()
            for line in a:
                f.write(line + "\n")


def main():
    root = tk.Tk()
    root.title("Bloggerson")
    app = App(root)
    app.pack()
    app.mainloop()


if __name__ == '__main__':
    main()
